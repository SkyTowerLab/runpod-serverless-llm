# Use PyTorch's CUDA-enabled base image
# FROM pytorch/pytorch:1.10-cuda11.1-cudnn8-runtime
FROM pytorch/pytorch:2.0.1-cuda11.7-cudnn8-runtime

WORKDIR /

# Install necessary libraries
RUN pip install runpod transformers

# Pre-download the model and tokenizer
# RUN python -c "from transformers import AutoTokenizer, AutoModelForCausalLM; \
#                model_id = 'TheBloke/Llama-2-13B-chat-GPTQ'; \
#                AutoTokenizer.from_pretrained(model_id).save_pretrained('/models/tokenizer/'); \
#                AutoModelForCausalLM.from_pretrained(model_id).save_pretrained('/models/model/')"

ENV TRANSFORMERS_CACHE /workspace/data

ADD handler.py .
ADD requirements.txt .
RUN pip install -r requirements.txt

CMD [ "python", "-u", "/handler.py" ]

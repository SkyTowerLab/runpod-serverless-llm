import runpod
import os
from threading import Thread
from transformers import AutoTokenizer, AutoModelForCausalLM, TextIteratorStreamer

model_id = os.environ.get("MODEL_ID", "TheBloke/Llama-2-7B-chat-GPTQ")
tokenizer = AutoTokenizer.from_pretrained(model_id)
model = AutoModelForCausalLM.from_pretrained(
    model_id,
    device_map="auto",
    max_position_embeddings=4096,
)


def generate_tokens(job):

    job_input = job["input"]
    prompt = job_input.pop("prompt")

    streamer = TextIteratorStreamer(tokenizer, skip_prompt=True)
    inputs = tokenizer(prompt, return_tensors='pt').to("cuda")
    kwargs = dict(
        input_ids=inputs.input_ids,
        streamer=streamer,
        max_new_tokens=job_input.get("max_new_tokens", 100),
        temperature=job_input.get("temperature", 0.7),
        top_k=job_input.get("top_k", 50),
        top_p=job_input.get("top_p", 0.95),
        repetition_penalty=job_input.get("repetition_penalty", 1.0),
        length_penalty=job_input.get("length_penalty", 1.0),
    )
    # generation_kwargs = dict(inputs, streamer=streamer, max_new_tokens=20)
    thread = Thread(target=model.generate, kwargs=kwargs)
    thread.start()

    for new_text in streamer:
        yield new_text


runpod.serverless.start({
    "handler": generate_tokens
})
